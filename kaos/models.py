""" Models """
from django.utils.translation import ugettext_lazy as _

from datetime import datetime
from random import randint

from django.db import models
from django.contrib.auth.models import User

from smart_selects.db_fields import ChainedForeignKey

class Organisation (models.Model):
	"""	A group of people representing an organisation, with the power to run and manage games """
	
	name = models.CharField(_("organisation name"), max_length=50,
		help_text=_("The name of the organisation."))
		
	slug = models.SlugField(_("slug"), unique=True,
		help_text=_("A unique indentifier (to be used in urls)."))
	
	link = models.URLField(_("organisation link"), blank=True,
		help_text=_("Link to information on the organisation."))
	
	email = models.EmailField(_("organisation email"),
		help_text=_("An email address for the organisation. This will be made public."))

	members = models.ManyToManyField(User, verbose_name=_("Representatives"), related_name="organisations",
		help_text=_("Representatives of the organisation."))
		
	# Meta
	
	class Meta:
		verbose_name = _('organisation')
		verbose_name_plural = _('organisations')
	
	def __unicode__ (self):
		return self.name
	
	@models.permalink
	def get_absolute_url(self):
		return ('organisation_detail', (), {'slug': self.slug})

class Game (models.Model):
	"""	An event, during which players can tag each other """
	
	# Game description
	
	title = models.CharField(_("Title"), max_length=50,
		help_text=_("The title of the event."))
		
	slug = models.SlugField(_("Slug"), unique=True,
		help_text=_("A unique indentifier (to be used in urls)."))
		
	description = models.TextField(verbose_name="Game information", blank=True,
		help_text=_('A full description of the game.<br />You can use markdown for formating: <a href="http://daringfireball.net/projects/markdown/basics">Markdown</a>. Any headers should be h3 or below.'))
	
	organisation = models.ForeignKey("Organisation", related_name='games',
		help_text=_("The organisation running the game."))
	
	# Start and end times for the event
	
	start_time = models.DateTimeField(_("Start time"),
		help_text=_("When does the game start?"))
	
	end_time = models.DateTimeField(_("End time"),
		help_text=_("When does the game end?"))
	
	time_period = lambda self: (self.start_time, self.end_time)
	time_period.short_description = _("Game period")
	
	active = lambda self: self.end_time > datetime.today()
	active.short_description = _("Active")
	
	@staticmethod
	def _status (start, end, detailed=False):
		"""	Returns a status string for the given time: one of 'inactive', 'active', 'complete'.
			detailed=True will return statuses appended with ' impending', if the status will change within 24h """
		
		today = datetime.today()
		
		if start > today:
			# If the game starts after today (has not started):
			status = 'inactive'
			if detailed and (start - today).days < 1:
				# If the game starts within 24 hours
				status += ' impending'
		elif end > today:
			# If the game ends after today (has started but not ended):
			status = 'active'
			if detailed and (end - today).days < 1:
				# If the game ends within 24 hours
				status += ' impending'
		else:
			# Else the game is in the past
			status = 'complete'
		return status
		
	status = lambda self: self._status(*self.time_period())
	status.short_description = _("Game status")
	
	# Signup period for the event
	
	signup_start_time = models.DateTimeField(_("Signup start time"),
		help_text=_("When can players start signing up?"))
	
	signup_end_time = models.DateTimeField(_("Signup end time"),
		help_text=_("When can players no longer sign up by?"))
	
	signup_time_period = lambda self: (self.signup_start_time, self.signup_end_time)
	signup_time_period.short_description = _("Signup period")
	
	signup_status = lambda self: self._status(*self.signup_time_period())
	signup_status.short_description = _("Signup status")
	
	# Relations
	
	teams = property(lambda self: self._score(self.team_set.all()),
		doc="Return a list of teams ranked by score.")
	
	users = models.ManyToManyField(User, through='Player', related_name='games',
		help_text=_("The users registered for the event."))
	
	players = property(lambda self: self._score(self.player_set.valid()),
		doc="Return the valid players ranked by score.")
	
	moderators = models.ManyToManyField(User, related_name='moderated_games',
		help_text=_("The users moderating the event."))
	
	# Game options
	
	signup_confirmation = models.BooleanField(_("Requires confirmation?"), default=True,
		help_text=_("Should signups be confirmed by an administrator?"))
	
	allow_pseudonym = models.BooleanField(_("Allow players to use a pseudonym?"), default=False,
		help_text=_("If true, players may hide their identity behind a pseudonym."))
	
	# Scoring
	
	recent_tags	= lambda self: self.tags.all().order_by('-time')[:25]
	
	tagger_points = models.BooleanField(_("Gain points for tags?"), default=True,
		help_text=_("Do tags made by the player count add to their score?"))
	
	tagged_points = models.BooleanField(_("Lose points for being tagged?"), default=False,
		help_text=_("Do tags where the player is tagged detract from their score?"))
	
	def _query_count_tags (self, queryset, tag_type):
		"""
		Return a query string counting the number of tags for a queryset
		
		Only Team and player querysets are supported.
		
		:Parameters:
			- `queryset` (QuerySet):
				The queryset to operate on.
				Must be on the `Team` or `Player` tables.
			- `tag_type` (str):
				'tagger', 'tagged' - returns the number of tags where
				the object tagged a player or was tagged by a player.
		"""
		query = "(select count(*) from {{tag}} where {where})"
		
		# Prepare the WHERE query
		if queryset.model == Player:
			where = "{tag}.{tag_type}_id={model}.id"
		elif queryset.model == Team:
			where = "{tag}.{tag_type}_team_id={model}.id"
		else:
			raise Exception("Invalid model.")
		
		# Return the query string
		return query.format(where=where).format(
			tag = Tag._meta.db_table,
			tag_type = tag_type,
			model = queryset.model._meta.db_table,
		)
			
	def _score (self, queryset):
		"""
		Annotate a queryset with it's score.
		
		The score is calculated from each objects points,
		and optionally the number of tags the made or were tagged by.
		"""
		
		# Always add the points variable of the object
		query = "points"
		
		# Add the number of tags where the object was the tagger?
		if self.tagger_points:
			query += " + " + self._query_count_tags(queryset, tag_type='tagger')
		
		# Remove the number of tags where the object was tagged?
		if self.tagged_points:
			query += " - " + self._query_count_tags(queryset, tag_type='tagged')
		
		return queryset.extra(
			select={'score': query},
			order_by=('-score',),
		)
	
	# Meta
	
	class Meta:
		verbose_name = _('game')
		verbose_name_plural = _('games')
	
	def __unicode__ (self):
		return self.title
	
	@models.permalink
	def get_absolute_url(self):
		return ('game_detail', (), {'slug': self.slug})

class Team (models.Model):
	"""	A set of players in a Game """
	game = models.ForeignKey(Game)
	
	name = models.CharField(_("Team name"), max_length=30,
		help_text=_("The team's name."))
	
	description = models.TextField(_("Description"), blank=True,
		help_text=_("A description of the team."))
	
	points = models.SmallIntegerField(_("Points"), default=0,
		help_text=_("Points that have been awarded to the team."))
	
	players = property(lambda self: self.game._score(self.player_set.valid()),
		doc="Return the valid players ranked by score.")
		
	# Meta
	
	class Meta:
		verbose_name = _('team')
		verbose_name_plural = _('teams')
	
	def __unicode__ (self):
		return self.name
	
	@models.permalink
	def get_absolute_url(self):
		return ('team_detail', (), {'slug': self.game.slug, 'id': self.id})

class PlayerManager(models.Manager):
	"""	Manager for the Player model """
	use_for_related_fields = True
	
	def valid (self):
		"""	Query only valid players """
		return self.get_query_set().filter(active=True, deleted=False)

class Player (models.Model):
	"""	A participant in a Game, and their status
		Intermediary model between the User and Game tables """
	
	objects = PlayerManager()
	
	# Relations
	
	game = models.ForeignKey(Game, verbose_name=_("game"),
		help_text=_("The game the player is taking part in."))
		
	user = models.ForeignKey(User, verbose_name=_("user"),
		help_text=_("The user that owns the player."))
	
	team = models.ForeignKey(Team, verbose_name=_("team"),
		null=True, blank=True,
		help_text=_("The team a player is on."))
	
	# Name
	
	pseudonym = models.CharField(_("pseudonym"), max_length=30, blank=True,
		help_text=_("A pseudonym, if the player wishes to remain anonymous."))
	
	def is_anon (self):
		"""	Is the user anonymous? """
		return self.game.allow_pseudonym and not self.pseudonym == ''
	
	def name (self):
		"""	Get a name for the user """
		return self.pseudonym if self.is_anon() else self.user.get_full_name()
		
	# Status
	
	deleted = models.BooleanField(_("Deleted"), default=False,
		help_text=_("True if the player has been removed from the game."))
	
	active = models.BooleanField(_("Active"), default=False,
		help_text=_("True if the player has been confirmed."))
	
	# Scoring
	
	lives = models.SmallIntegerField(default=0,
		help_text=_("The number of lives a player has"))
	
	points = models.SmallIntegerField(default=0,
		help_text=_("Points that have been awarded to the player"))
	
	# Player token
	
	token = models.SmallIntegerField(unique=True, null=True, blank=True,
		help_text=_("The players token."))
	
	@staticmethod
	def generate_token ():
		token = ""
		for i in xrange(10):
			token += str(randint(1,9))
		return token
	
	def save (self):
		"""	Create a token for the player if they do not have one.
			New tokens can be generated by seting Player.token to None """
		if not self.token:
			self.token = self.generate_token()
		super(Player, self).save()
	
	# Meta
	
	class Meta:
		verbose_name = _('player')
		verbose_name_plural = _('players')
	
	def __unicode__ (self):
		return self.name()
	
	@models.permalink
	def get_absolute_url(self):
		return ('player_detail', (), {'slug': self.game.slug, 'id': self.id})

class Tag (models.Model):
	"""	A record of a player tagging another player, and the team each player was a member of at the time.
		The 'tagger' and 'tagged' fields are chained to the Game, so that only players in the game can be selected. """
	
	game = models.ForeignKey(Game, verbose_name=_("game"), related_name='tags',
		help_text=_("The game the tag took place in."))
	
	tagger = ChainedForeignKey(Player, verbose_name=_("tagger"),
		chained_field="game", chained_model_field="game", related_name='tags',
		help_text=_("The player who tagged the other player."))
	tagger_team = ChainedForeignKey(Team, verbose_name=_("tagger's team"),
		chained_field="game", chained_model_field="game", related_name='tags', null=True, blank=True,
		help_text=_("The team of the tagger."))
	
	tagged		= ChainedForeignKey(Player, verbose_name=_("tagged player"),
		chained_field="game", chained_model_field="game", related_name='tagged',
		help_text=_("The player who was tagged by the other player."))
	tagged_team	= ChainedForeignKey(Team, verbose_name=_("tagged player's team"),
		chained_field="game", chained_model_field="game", related_name='tagged', null=True, blank=True,
		help_text=_("The team of the tagged player."))
		
	time = models.DateTimeField(_("time"),
		help_text=_("The time the tag took place."))
	
	# Meta
	
	class Meta:
		verbose_name = _('tag')
		verbose_name_plural = _('tags')
	
	def __unicode__ (self):
		team_description = _(" of team {team}")
		return _("{tagger}{tagger_team} tagged {tagged}{tagged_team}").format(
			tagger      = self.tagger.name(),
			tagged      = self.tagged.name(),
			tagger_team = team_description.format(team=self.tagger_team) if self.tagger_team else "",
			tagged_team = team_description.format(team=self.tagged_team) if self.tagged_team else "",
		)
	
	@models.permalink
	def get_absolute_url(self):
		return ('tag_detail', (), {'slug': self.game.slug, 'id': self.id})
