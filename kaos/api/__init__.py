"""	API versions """

import os

def import_all_modules ():
	""" Import all modules in the directory """
	modules = []
	for filename in os.listdir(os.path.dirname(__file__)):
		# Ignore __init__.py and files that are not python modules
		if filename == '__init__.py' or filename[-3:] != '.py':
			continue
		# Add the module if it has a meta variable
		module_name = '.'.join((__package__, filename[:-3]))
		module = __import__(module_name, locals(), globals(), ['meta'])
		# Return the module_name and module
		modules.append((module_name, module))
	return modules

# Import api versions

from django.conf.urls.defaults import patterns, include, url

from tastypie.api import Api

apis = []
for module_name, module in import_all_modules():
	# Create the api object
	name = module_name.split('.')[-1]
	api = Api(api_name=name)
	# Register each of the api's resources to the resource
	for resource in module.__all__:
		api.register(resource())
	# Add the api to the list of urls
	apis.append(api)

# Add urls for each of the apis
urlpatterns = patterns('', *[url(r'^api/', include(api.urls)) for api in apis])
