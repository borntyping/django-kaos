"""
API, version 0
"""

from datetime import datetime

from django.contrib.auth.models import User

from tastypie import fields
from tastypie.api import Api
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS

from kaos.models import *

# Constant for filters that require an exact match
EXACT = ('exact',)

class DefaultMeta (object):
	"""	Default settings for resources """
	allowed_methods = ['get']

class UserResource(ModelResource):
	class Meta (DefaultMeta):
		queryset = User.objects.filter(is_active=True)
		fields = ['id', 'username']
        
class GameResource (ModelResource):
	class Meta (DefaultMeta):
		queryset = Game.objects.all()
		resource_name = 'game'
		filtering = {'id': EXACT, 'slug': ALL_WITH_RELATIONS}
		include_absolute_url = True
		
	def get_object_list(self, request):
		return super(GameResource, self).get_object_list(request).filter(end_time__gte=datetime.today())

class TeamResource(ModelResource):
	game = fields.ToOneField(GameResource, 'game',
		help_text=Team.game.field.help_text)

	class Meta (DefaultMeta):
		queryset = Team.objects.all()
		filtering = {"game": EXACT}
		
class PlayerResource(ModelResource):
	game = fields.ToOneField(GameResource, 'game',
		help_text=Player.game.field.help_text)
		
	user = fields.ToOneField(UserResource, 'user',
		help_text=Player.user.field.help_text)
		
	def dehydrate_user (self, bundle):
		"""	If the user is using a psuedonym, remove their details from the bundle """
		bundle.data['anon'] = bundle.obj.is_anon()
		if bundle.obj.is_anon():
			return None
		else:
			return bundle.data['user']

	class Meta (DefaultMeta):
		queryset = Player.objects.filter(deleted=False, active=True)
		resource_name = 'player'
		fields = ['game', 'name', 'lives', 'points']
		filtering = {'game': ALL_WITH_RELATIONS}

class TagResource(ModelResource):
	game = fields.ToOneField(GameResource, 'game',
		help_text=Tag.game.field.help_text)
	
	tagger = fields.ToOneField(PlayerResource, 'tagger', full=True,
		help_text=Tag.tagger.field.help_text)
	tagger_team = fields.ToOneField(TeamResource, 'tagger_team', full=True, blank=True, null=True,
		help_text=Tag.tagger_team.field.help_text)
	
	tagged = fields.ToOneField(PlayerResource, 'tagged', full=True,
		help_text=Tag.tagged.field.help_text)
	tagged_team = fields.ToOneField(TeamResource, 'tagged_team', full=True, blank=True, null=True,
		help_text=Tag.tagged_team.field.help_text)

	class Meta (DefaultMeta):
		queryset = Tag.objects.all()
		filtering = {
			"game": EXACT,
			"tagger": EXACT, "taggerTeam": EXACT,
			"tagged": EXACT, "taggedTeam": EXACT,
		}

__all__ = [UserResource, GameResource, TeamResource, PlayerResource, TagResource]
		