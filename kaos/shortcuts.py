""" Helper functions """

from django.http import Http404
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

def paginate (request, queryset, num=10):
	paginator = Paginator(queryset, num)
	try:
		return paginator.page(request.GET.get('page'))
	except PageNotAnInteger:
		return paginator.page(1)
	except EmptyPage:
		return paginator.page(paginator.num_pages)

def object_or_404 (func):
	"""	Decorate a function to raise a 404 error if any object is not found """
	def decorated_object_or_404 (*args, **kwargs):
		try:
			return func(*args, **kwargs)
		except ObjectDoesNotExist:
			raise Http404
	return decorated_object_or_404

def detail_templates (obj, title=None):
	"""
	Returns a list of templates for a given object
	
	:Parameters:
		- `obj` (django.db.models.Model): A model instance
	
	:Keywords:
		- `title` (str): A title (such as the objects name or slug) unique to the instances
	"""
	templates = ['kaos/{model}/unique/{title}.html', 'kaos/{model}/detail.html']
	details = {'model': obj.__class__.__name__.lower(), 'title': title}
	return [t.format(**details) for t in templates]
	
def get_object_or_none (cls, **kwargs):
	"""
	Return a object for the given query arguments,
	or None if not such object exists
	
	:Parameters:
		- `cls` (django.db.models.Model): A model instance
	"""
	try:
		return cls.objects.get(**kwargs)
	except cls.DoesNotExist:
		return None
