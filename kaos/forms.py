from django.utils.translation import ugettext_lazy as _
from django import forms

class FullnameForm (forms.Form):
	"""
	A form that adds a fullname to a user.
	
	Used to extend the allauth signup form - see README for more details.
	"""
	firstname = forms.CharField(label=_("First name"))
	lastname = forms.CharField(label=_("Last name"))

	def save (self, user):
		"""	Save the fullname to the user """
		user.first_name = self.cleaned_data['firstname']
		user.last_name = self.cleaned_data['lastname']
		user.save()
