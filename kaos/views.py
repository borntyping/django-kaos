"""	Views """

from datetime import datetime

from django.http import HttpResponse, Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.views.generic import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.utils.decorators import method_decorator

from kaos.shortcuts import paginate, object_or_404, get_object_or_none, detail_templates
from kaos.models import User, Organisation, Game, Team, Player, Tag
from kaos.signals import *

class LoginRequiredMixin (object):
	"""	A View mixin that applies the login_required decorator to the view """

	@method_decorator(login_required)
	def dispatch(self, request, *args, **kwargs):
		return super(LoginRequiredMixin, self).dispatch(request, *args, **kwargs)

class GenericListView (ListView):
	paginate_by = 10
	
	def get_template_names (self):
		"""	Return the template name, based on the application and model names. """
		return ['{app}/{model}/list.html'.format(**{
			'app': self.object_list.model._meta.app_label,
			'model': self.object_list.model._meta.object_name.lower(),
		})]

class GenericDetailView (DetailView):
	"""	A detail view that generates a list of templates from the selected object """
	
	templates = ['{app}/{model}/unique/{id}.html', '{app}/{model}/detail.html']
	def get_template_names (self):
		"""	Get the template names for an object,
			based on the objects application, model and unique id. """
		details = {
			'app': self.object._meta.app_label,
			'model': self.object._meta.object_name.lower(),
			'id': getattr(self.object, getattr(self, 'id_field', 'id')),
		}
		return [t.format(**details) for t in self.templates]

class GameMixin (object):
	@property
	def game (self):
		""" Return and cache the game described by the url's slug """
		if not hasattr(self, '_game'):
			self._game = Game.objects.get(slug=self.kwargs['slug'])
		return self._game
	
	def get_context_data (self, **kwargs):
		context = super(GameMixin, self).get_context_data(**kwargs)
		context['game'] = self.game
		context['player'] = get_object_or_none(Player,
			game__slug=self.game.slug, user=self.request.user)
		return context

class GameDetailView (GameMixin, GenericDetailView):
	"""	A view showing an object related to a game """
	pass

class GameListView (GameMixin, GenericListView):
	""" A view showing a list of objects related to a game """
	pass

# -------------------------
# Homepage
# -------------------------

class Homepage(TemplateView):
    template_name = 'kaos/homepage.html'

# -------------------------
# Users
# -------------------------

class UserDetail (LoginRequiredMixin, GenericDetailView):
	queryset = User.objects.all()
	context_object_name = 'profile'
	
	@object_or_404
	def get_object (self):
		username = self.kwargs.get('username', self.request.user.username)
		return User.objects.get(username=username)

	def get_context_data (self, **kwargs):
		context = super(UserDetail, self).get_context_data(**kwargs)
		context['games'] = self.object.games.filter(player__deleted=False, player__active=True)
		context['is_self'] = (self.object == self.request.user)
		return context

# -------------------------
# Organisation
# -------------------------

class OrganisationList (GenericListView):
	queryset = Organisation.objects.all()
	context_object_name = 'organisations'

class OrganisationDetail (GenericDetailView):
	queryset = Organisation.objects.all()
	context_object_name = 'organisation'

# -------------------------
# Games
# -------------------------

class GameList (GenericListView):
	context_object_name = 'games'
	
	#: The name of a filter to apply to the list, relative to the current time.
	#: Examples: ``end_time__gt``, ``start_time__lt``.
	filter_name = None
	
	#: The title to show on the page
	title = "Games index"
	
	def get_queryset (self):
		kwargs = {self.filter_name: datetime.today()} if self.filter_name else {}
		return Game.objects.filter(**kwargs)
	
	def get_context_data (self, **kwargs):
		context = super(GameList, self).get_context_data(**kwargs)
		context['title'] = self.title
		return context

class GameDetail (GenericDetailView):
	queryset = Game.objects.all()
	context_object_name = 'game'
	
	def get_context_data (self, **kwargs):
		context = super(GameDetail, self).get_context_data(**kwargs)
		context['player'] = get_object_or_none(Player, game=self.object, user=self.request.user)
		return context

# -------------------------
# Teams
# -------------------------

class TeamList (GameListView):
	context_object_name = 'teams'
	get_queryset = lambda self: self.game.teams.all()
		
class TeamDetail (GameDetailView):
	context_object_name = 'team'
	
	@object_or_404
	def get_object (self):
		return self.game.teams.get(id=self.kwargs['id'])

# -------------------------
# Players
# -------------------------

class PlayerList (GameListView):
	context_object_name = 'players'
	get_queryset = lambda self: self.game.players.all()

class PlayerDetail (GameDetailView):
	context_object_name = 'player'
	
	@object_or_404
	def get_object (self):
		return self.game.players.get(id=self.kwargs['id'])
	
	def get_context_data (self, **kwargs):
		context = super(GameDetailView, self).get_context_data(**kwargs)
		print context
		return context

# -------------------------
# Tags
# -------------------------
	
class TagList (GameListView):
	context_object_name = 'tags'
	get_queryset = lambda self: self.game.tags.all()
	paginate_by = 25

class TagDetail (GameDetailView):
	context_object_name = 'tag'
	
	@object_or_404
	def get_object (self):
		return self.game.tags.get(id=self.kwargs['id'])
