from django.conf.urls.defaults import patterns, include, url
from django.conf import settings

from kaos.views import UserDetail, OrganisationDetail, GameDetail, TeamDetail, PlayerDetail, TagDetail
from kaos.views import OrganisationList, GameList, TeamList, PlayerList, TagList
from kaos.views import Homepage
	
# Application urls
urlpatterns = patterns('',
	# Smart select urls
	url(r'^chaining/', include('smart_selects.urls')),
	
	# Users
	url(r'^profile/(?P<username>.+)/', UserDetail.as_view(), name='user_detail'),
	url(r'^profile/', UserDetail.as_view(), name='user_detail'),

	# Organisations
	url(r'^organisations/(?P<slug>[\w-]+)/', OrganisationDetail.as_view(), name='organisation_detail'),
	url(r'^organisations/', OrganisationList.as_view(), name='organisation_list'),
	
	# Games
	url(r'^games/', include(patterns('',
		url(r'^$',		GameList.as_view(title="Active games", filter_name='end_time__gt'), name='games_list'),
		url(r'^past/$',	GameList.as_view(title="Past games",   filter_name='end_time__lt'), name='games_list_past'),
		url(r'^all/$',	GameList.as_view(title="All games"), name='games_list_all'),
		url(r'^(?P<slug>[\w-]+)/', GameDetail.as_view(), name='game_detail'),
	))),
	
	# Teams
	url(r'^teams/(?P<slug>[\w-]+)/', include(patterns('',
		url(r'^$', TeamList.as_view(), name='team_list'),
		url(r'^(?P<id>\d+)/', TeamDetail.as_view(), name='team_detail'),
	))),
	
	# Players
	url(r'^players/(?P<slug>[\w-]+)/', include(patterns('',
		url(r'^$', PlayerList.as_view(), name='player_list'),
		url(r'^(?P<id>\d+)/', PlayerDetail.as_view(), name='player_detail'),
	))),
	
	# Tags
	url(r'^tags/(?P<slug>[\w-]+)/', include(patterns('',
		url(r'^$', TagList.as_view(), name='tag_list'),
		url(r'^(?P<id>\d+)/', TagDetail.as_view(), name='tag_detail'),
	))),
	
	# Homepage
	url(r'^$', Homepage.as_view(), name='kaos_home'),
)

# Only load api if tastypie is availible
if 'tastypie' in settings.INSTALLED_APPS:
	from kaos.api import urlpatterns as api_urls
	urlpatterns += api_urls
