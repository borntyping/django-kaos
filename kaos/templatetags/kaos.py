""" Template tags """

from datetime import datetime

from django import template
from django.core.urlresolvers import reverse
from django.contrib.sites.models import Site
from django.utils.timesince	import timesince, timeuntil
from django.utils.translation import ugettext as _

register = template.Library()

"""	Show the name of the current site """
@register.simple_tag
def site_name (): return Site.objects.get_current().name

"""	Show a link to return to an object """
@register.inclusion_tag('inline_return_link.html')
def return_link (obj): return {'object': obj}

"""	Show pagination links """
@register.inclusion_tag('inline_paginate.html', takes_context=True)
def show_page_links (context):
	return dict((k,context[k]) for k in ['is_paginated', 'page_obj', 'paginator'])
	
# -------------------------
# Users
# -------------------------

"""	Show a list of users """
@register.inclusion_tag('auth/user/inline_list.html')
def show_users (users): return {'users': users}

# -------------------------
# Games
# -------------------------

"""	Show a list of games """
@register.inclusion_tag('kaos/game/inline_list.html')
def show_games (games): return {'games': games}

"""	Show a list of games categorised by status """
@register.inclusion_tag('kaos/game/inline_list_status.html')
def show_games_by_status (games):
	now = datetime.today()
	return {'games': {
		'inactive': games.filter(start_time__gt=now),
		'active':   games.filter(start_time__lt=now, end_time__gt=now),
		'complete': games.filter(end_time__lt=now),
	}}

"""	Show the progress of a game period """
@register.inclusion_tag('kaos/game/progress.html')
def show_progress (game, period):
	return {
		'game':   game,
		'status': game._status(*period),
		'start':  period[0],
		'end':    period[1],
	}

"""	Show a small status label """
@register.inclusion_tag('kaos/game/progress_status.html')
def show_status (status): return {'status': status}

"""	Show signup progress relative to the game progress """
@register.inclusion_tag('kaos/game/progress_signup.html')
def show_signup_progress (game):
	status = game.signup_status()
	# Signup has already closed
	if status == 'complete':
		message = _("Signup has closed.")
	# Signup during the game
	elif game.time_period() == game.signup_time_period():
		message = _("Signup is open during the game.")
	# If signup has yet to start
	elif status == 'inactive':
		message = _("Signup opens in {0} and closes {1} later").format(
			timeuntil(game.signup_start_time),
			timeuntil(game.signup_end_time, now=game.signup_start_time))
	# Signup is active
	else:
		# Signup closes when the game *starts*
		if game.signup_end_time == game.start_time:
			message = _("Signup closes at the start of the game.")
		# Signup closes when the game *ends*
		elif game.signup_end_time == game.end_time:
			message = _("Signup closes at the end of the game.")
		# Signup has not closed yet
		else:
			message = _("Signup closes in {0}").format(timeuntil(game.signup_end_time))
	return {'game': game, 'status': status, 'message': message}

# -------------------------
# Teams
# -------------------------

"""	Show a list of teams """
@register.inclusion_tag('kaos/team/inline_list.html')
def show_teams (teams): return {'teams': teams}

# -------------------------
# Players
# -------------------------

"""	Show a list of players """
@register.inclusion_tag('kaos/player/inline_list.html')
def show_players (players): return {'players': players}

# -------------------------
# Tags
# -------------------------

"""	Show a text description of a tag """
@register.simple_tag
def show_tag_text (tag):
	return "{} tagged {}".format(tag.tagger.name(), tag.tagged.name())

"""	Show a description of a tag """
@register.inclusion_tag('kaos/tag/inline.html')
def show_tag (tag, show_teams=True):
	return {'tag': tag, 'show_teams': show_teams}

"""	Show a list of tags """
@register.inclusion_tag('kaos/tag/inline_list.html')
def show_tags (tags): return {'tags': tags}

"""	Show a list of tags, split into a tagger and tagged group. """
@register.inclusion_tag('kaos/tag/inline_list_split.html')
def show_tags_split (tagger, tagged):
	return {'tagger': tagger, 'tagged': tagged}
