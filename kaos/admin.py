""" Admin models """

from datetime import datetime
from django.contrib import admin
from models import *

class OrganisationAdmin (admin.ModelAdmin):
	list_display = ('name', 'slug', 'email')
	fieldsets = [
		('Description',		{'fields': ['name', 'slug', 'link', 'email']}),
		(None,				{'fields': ['members']}),
	]
	
class GameAdmin (admin.ModelAdmin):
	list_display = ('title', 'organisation', 'status', 'start_time', 'end_time', 'signup_start_time', 'signup_end_time')
	list_filter = ['start_time', 'end_time']
	fieldsets = [
		('Description',		{'fields': ['title', 'slug', 'description', 'organisation']}),
		('Event period',	{'fields': ['start_time', 'end_time']}),
		('Signup period',	{'fields': ['signup_start_time', 'signup_end_time', 'signup_confirmation']}),
		('Moderation',		{'fields': ['moderators', 'allow_pseudonym']}),
		('Scoring',			{'fields': ['tagger_points', 'tagged_points']}),
	]
	prepopulated_fields = {"slug": ("title",)}

class TeamAdmin (admin.ModelAdmin):
	list_display = ('name', 'game')
	list_filter = ['game']
	fieldsets = [
		(None,				{'fields': ['name', 'description', 'points', 'game']}),
	]
	
class PlayerAdmin (admin.ModelAdmin):
	list_display = ('name', 'team', 'active', 'deleted', 'game')
	list_filter = ['game', 'team']
	fieldsets = [
		(None,				{'fields': ['user', 'game']}),
		('Status',			{'fields': ['lives', 'points', 'team',]}),
		('Attributes',		{'fields': ['pseudonym', 'token', 'active', 'deleted']}),
	]
	
class TagAdmin (admin.ModelAdmin):
	list_display = ('id', '__unicode__', 'time', 'game')
	list_filter = ['game', 'time']
	fieldsets = [
		(None,		{'fields': ['time', 'game']}),
		('Tagger',	{'fields': ['tagger', 'tagger_team']}),
		('Tagged',	{'fields': ['tagged', 'tagged_team']}),
	]

for model in ['Organisation', 'Game', 'Team', 'Player', 'Tag']:
	admin.site.register(globals()[model], globals()[model + 'Admin'])
