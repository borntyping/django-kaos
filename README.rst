====
KAOS
====

``django-kaos`` is an tool for managing KAOS games (`link <http://en.wikipedia.org/wiki/Assassin_(game)>`_).

The tool provides a system for managing a game of this style. Players are each given a token (a 10 digit number). Upon being tagged by another player, the tagged player gives this token to the player that tagged them, who can then input this token on the site to register a tag between those two players. 

Extensions
==========

Applications that extend the capability of django-kaos:

- ``django-kaos-avatar`` (`github <https://github.com/borntyping/django-kaos-avatar>`_) - Show avatars for users, players and teams
- ``django-kaos-location`` - Store locations with tags.

Requirements
============

``django-kaos`` requires the following python packages to be installed:

- ``django`` - 1.3 or greater, though ``django-kaos-avatar`` requires 1.4
- ``django-smart-selects``

Optional Requirements
---------------------

- ``django-tastypie`` - Provides an api for the applications data.
- ``lxml`` - Allows ``django-tastypie`` to format output as xml.

Installation
============

From source:

::

	pip install django-smart-selects django-tastypie
	git clone https://github.com/borntyping/django-kaos.git
	cd django-kaos
	python setup.py install

In ``settings.py``:

::

	INSTALLED_APPS = (
		...
		'kaos',
		'tastypie',
	)

``tastypie`` is only required if you wish to provide an api.

User model
----------

To use kaos to show user detail pages for the User model, place these setttings in `settings.py`:

::

	from django.core.urlresolvers import reverse, reverse_lazy
	LOGIN_REDIRECT_URL = reverse_lazy('user_detail')

	ABSOLUTE_URL_OVERRIDES = {
		'auth.user': lambda u: reverse('user_detail', kwargs={'username': u.username}),
	}
