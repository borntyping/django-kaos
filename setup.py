#!/usr/bin/python

from setuptools import setup, find_packages

setup(
	name             = 'django-kaos',
	version          = '1.0dev',

	author           = 'Sam Clements',
	author_email     = 'sam@borntyping.co.uk',

	url              = 'https://github.com/borntyping/django-kaos',
	description      = 'A django application for managing kaos games',
	long_description = open('README.rst').read(),

	packages         = find_packages(exclude=['migrations']),
	install_requires = ['django>=1.3', 'django-smart-selects==1.0.4'],
	extras_require   = {'API': 'django-tastypie==9.1.1', 'API-XML': 'lxml'},
	dependency_links = ['http://github.com/toastdriven/django-tastypie/tarball/master#egg=django-tastypie-9.1.1'],
)
